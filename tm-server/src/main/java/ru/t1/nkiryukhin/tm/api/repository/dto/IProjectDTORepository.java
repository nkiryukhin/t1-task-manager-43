package ru.t1.nkiryukhin.tm.api.repository.dto;

import ru.t1.nkiryukhin.tm.dto.model.ProjectDTO;

public interface IProjectDTORepository extends IUserOwnedDTORepository<ProjectDTO> {
}
