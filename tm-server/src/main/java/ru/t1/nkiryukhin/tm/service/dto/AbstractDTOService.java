package ru.t1.nkiryukhin.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.api.repository.dto.IDTORepository;
import ru.t1.nkiryukhin.tm.api.service.IConnectionService;
import ru.t1.nkiryukhin.tm.api.service.dto.IDTOService;
import ru.t1.nkiryukhin.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;

public abstract class AbstractDTOService<M extends AbstractModelDTO, R extends IDTORepository<M>> implements IDTOService<M> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractDTOService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    protected EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @NotNull
    protected abstract IDTORepository<M> getRepository(@NotNull final EntityManager entityManager);

    @Nullable
    @Override
    public M add(@Nullable final M model) {
        if (model == null) return null;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDTORepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @Override
    public void removeOne(@Nullable final M model) {
        if (model == null) return;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDTORepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.remove(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void update(@Nullable final M model) {
        if (model == null) return;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDTORepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
