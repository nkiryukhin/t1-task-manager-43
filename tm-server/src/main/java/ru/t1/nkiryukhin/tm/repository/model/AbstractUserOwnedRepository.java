package ru.t1.nkiryukhin.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.nkiryukhin.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.nkiryukhin.tm.model.AbstractUserOwnedModel;

import javax.persistence.EntityManager;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    protected AbstractUserOwnedRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

}
